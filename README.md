# Android Assignment CS
This is a placeholder README file with the instructions for the assingment. We expect you to build your own README file.

## Instructions

You can find all the instrucrtions for the assingment on [Assingment Instructions](https://docs.google.com/document/d/1zCIIkybu5OkMOcsbuC106B92uqOb3L2PPo9DNFBjuWg/edit?usp=sharing).

## Delivering the code
* Fork this repo and select the access level as private **[Check how to do it here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)**
* Go to settings and add the user **m-cs-recruitment@backbase.com** with the read access **[Check how to do it here](https://confluence.atlassian.com/bitbucket/grant-repository-access-to-users-and-groups-221449716.html)**
* Send an e-mail to **m-cs-recruitment@backbase.com** with your info and repo once the development is done

Please remember to work with small commits, it help us to see how you improve your code :)


//NOTES

1. USED THIRD -PARTY SUCH AS :
    A. RETROFIT OKHTTP - FOR API CALLS
    B. DAGGER - DEPENDENCY INJECTIONS
    C. GLIDE - IMAGE RENDERING WITH CACHING
    D. RXJAVA/RXANDROID - REACTIVE CODING
    E. TAG VIEW - UI FOR MOVEI DETAILS GENRE
    F. GLIDE FOR IMAGE, IT HAS IMAGE CACHING AS DEFAULT
    G. SOME UI LIBRARIES FOR DIMENS
2. ARCHITECTURE : MVP

3. ADDED CACHE FOR PLAYING MOVIES/ PAGE 1 OF POPULAR MOVIES
4.
