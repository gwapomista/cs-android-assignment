package com.backbase.assignment.base

import android.app.Activity
import android.app.Application
import android.app.Service
import com.backbase.assignment.BuildConfig
import com.backbase.assignment.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import timber.log.Timber
import javax.inject.Inject

class BaseApp : Application(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    override fun onCreate() {
        super.onCreate()

        instance = this

        /** initialize timber  */
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        /** initialize dagger */
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    override fun serviceInjector(): AndroidInjector<Service> {
        return dispatchingServiceInjector
    }

    companion object {
        private var instance: BaseApp? = null

        fun applicationContext() : BaseApp {
            return instance as BaseApp
        }
    }
}