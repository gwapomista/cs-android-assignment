package com.backbase.assignment.base

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment

open class BaseFragment: Fragment, BaseView {

    val FRAGMENT_PARAM = "fragment_param"
    lateinit var baseActivity: BaseActivity

    constructor() : super()

    constructor(layoutRes: Int) : super(layoutRes)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseActivity = activity as BaseActivity
    }

    override fun getStringResource(stringId: Int): String {
        return baseActivity.getStringResource(stringId)
    }

    override fun getColourResource(stringId: Int): Int {
        return Color.parseColor(this.getStringResource(stringId))
    }

    fun getDefaultParam(): String? {
        arguments?.let {
            return it.getString(FRAGMENT_PARAM)
        }
        return ""
    }

    fun getParam(key: String): String? {
        arguments?.let {
            return it.getString(key)
        }
        return ""
    }

    companion object {
        @JvmStatic
        fun newInstance(defaultParam: String) =
                BaseFragment().apply {
                    arguments = Bundle().apply {
                        putString(FRAGMENT_PARAM, defaultParam)
                    }
                }
    }
}