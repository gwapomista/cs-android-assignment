package com.backbase.assignment.base

import androidx.lifecycle.LifecycleOwner

interface BaseView : LifecycleOwner {

    fun getStringResource(stringId: Int): String

    fun getColourResource(stringId: Int) : Int

}
