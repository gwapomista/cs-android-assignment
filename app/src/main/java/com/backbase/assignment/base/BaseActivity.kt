package com.backbase.assignment.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import com.backbase.assignment.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


open class
BaseActivity : AppCompatActivity(), BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    public override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAfterTransition()
    }


    //Resource Utilities

    override fun getStringResource(stringId: Int): String {
        return resources.getString(stringId)
    }

    override fun getColourResource(stringId: Int): Int {
        return Color.parseColor(this.getStringResource(stringId))
    }


    //Fragment Utilities

    fun addFragment(fragment: BaseFragment, fragmentContainerResId: Int) {
        supportFragmentManager
                .beginTransaction()
                .replace(fragmentContainerResId, fragment)
                .addToBackStack(null)
                .commit()
    }

    fun replaceFragment(fragment: BaseFragment, fragmentContainerResId: Int) {
        supportFragmentManager
                .beginTransaction()
                .replace(fragmentContainerResId, fragment)
                .addToBackStack(null)
                .commit()
    }



    //Activity Transitions

    fun animateToLeft(activity: Activity) {
        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left)
    }

    fun animateToRight(activity: Activity) {
        activity.overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right)
    }

    fun moveToOtherActivity(clz: Class<*>) {
        startActivity(Intent(this, clz))
        animateToLeft(this)
    }

    fun moveToOtherActivityForResult(clz: Class<*>, requestCode: Int) {
        startActivityForResult(Intent(this, clz), requestCode)
        animateToLeft(this)
    }

    fun moveToOtherActivityWithSharedElements(clz: Class<*>, view: View, transitionName: String) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, transitionName)
        startActivity(Intent(this, clz), options.toBundle())
    }

    fun moveToOtherActivityWithSharedElements(clz: Class<*>, options: ActivityOptionsCompat) {
        startActivity(Intent(this, clz), options.toBundle())
    }

    fun toast(message : String){
        Toast.makeText(this,message,Toast.LENGTH_SHORT)
    }
}