package com.backbase.assignment.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.text.InputFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.backbase.assignment.R
import kotlinx.android.synthetic.main.custom_rating_view.view.*
import java.util.ArrayList

class RatingView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (attrs == null) return
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context, attrs, defStyleAttr) {

        if (attrs == null) return
        init(context)
    }

    private fun init(context: Context) {
        val inflater = context.
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        inflater.inflate(R.layout.custom_rating_view, this)
    }

    fun setRating(rating: Double){
        tvRating.text = (rating * 10).toInt().toString()

        if (rating>=5){
            progressRating.progressDrawable = context.getDrawable(R.drawable.progress_green)
            progressRating.setBackgroundResource(R.drawable.ring_green)
        }else{
            progressRating.progressDrawable = context.getDrawable(R.drawable.progress_yellow)
            progressRating.setBackgroundResource(R.drawable.ring_yellow)
        }

        progressRating.progress = rating.toInt()
    }
}