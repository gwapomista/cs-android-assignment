package com.backbase.assignment.ui.main.mvp

import com.backbase.assignment.base.BasePresenter
import com.backbase.assignment.data.prefs.CacheManager
import com.backbase.assignment.di.api.ApiService
import com.jakewharton.rxrelay2.BehaviorRelay
import com.backbase.assignment.ui.main.MainActivity
import com.backbase.assignment.util.scheduler.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainPresenter(view: MainContract.View,
                    private val apiService: ApiService,
                    private val cacheManager: CacheManager):

    BasePresenter<MainContract.View>(view), MainContract.Presenter {

    var shouldStopLoading = false;

    override fun getCachedMovies() {
        val cachedPopularMovies = cacheManager.getPopularMoviesCache()
        val cachedPlayingMovies = cacheManager.getPlayingMoviesCache()

        if (cachedPopularMovies.isNotEmpty()){
            view.displayCachedPopularMovies(cachedPopularMovies)
        }

        if (cachedPlayingMovies.isNotEmpty()){
            view.displayCachedPlayingMovies(cachedPlayingMovies)
        }
    }

    override fun fetchPlayingMovies() {
        val disposable = apiService.getPlayingMovies()
            .map { it.results }
            .subscribeBy(
                onSuccess = {
                    cacheManager.savePlayingMovies(it)
                    view.showPlayingMovies(it)
                },
                onError = { Timber.w(it) }
            )

        addDisposable(disposable)
    }

    override fun fetchPopularMovies(page : Int) {
        if (shouldStopLoading){
            return
        }

        val disposable = apiService.getPopularMovies(page)
            .map { it.results }
            .subscribeBy(
                onSuccess = {
                    if (it.isEmpty() || it.size < 20){
                        shouldStopLoading = true
                    }

                    if (page == 1){
                        cacheManager.savePopularMovies(it)
                        view.showPopularMovies(it)
                    }else{
                        view.addPopularMovies(it)
                    }
                },
                onError = { Timber.w(it) }
            )

        addDisposable(disposable)
    }

}
