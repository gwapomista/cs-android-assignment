package com.backbase.assignment.ui.main.di

import com.backbase.assignment.data.prefs.CacheManager
import com.backbase.assignment.di.api.ApiService
import com.backbase.assignment.ui.main.MainActivity
import com.backbase.assignment.ui.main.mvp.MainContract
import com.backbase.assignment.ui.main.mvp.MainPresenter
import com.backbase.assignment.util.scheduler.AppSchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    internal fun provideView(activity: MainActivity): MainContract.View {
        return activity
    }

    @Provides
    internal fun providePresenter(view: MainContract.View,
                                  apiService: ApiService,
                                  cacheManager: CacheManager): MainPresenter {

        return MainPresenter(view,apiService,cacheManager)
    }
}
