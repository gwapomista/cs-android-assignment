package com.backbase.assignment.ui.main.mvp

import com.backbase.assignment.base.BaseView
import com.backbase.assignment.data.api.reponsemodels.Movie

interface MainContract {
    interface View : BaseView {
        fun showPlayingMovies(movies : List<Movie>)
        fun showPopularMovies(movies: List<Movie>)
        fun addPopularMovies(movies: List<Movie>)

        fun displayCachedPopularMovies(movies : List<Movie>)
        fun displayCachedPlayingMovies(movies : List<Movie>)
    }

    interface Presenter {
        fun getCachedMovies()
        fun fetchPlayingMovies()
        fun fetchPopularMovies(page : Int)
    }
}
