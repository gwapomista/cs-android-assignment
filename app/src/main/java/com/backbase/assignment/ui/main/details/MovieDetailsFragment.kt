package com.backbase.assignment.ui.main.details;

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.backbase.assignment.R
import com.backbase.assignment.base.BaseFragment
import com.backbase.assignment.data.api.reponsemodels.Movie
import com.backbase.assignment.data.api.reponsemodels.MovieDetails
import com.backbase.assignment.data.api.reponsemodels.getDisplayReleaseDate
import com.backbase.assignment.data.api.reponsemodels.getPosterLink
import com.backbase.assignment.ui.main.MainActivity
import com.backbase.assignment.ui.main.details.mvp.MovieDetailsContract
import com.backbase.assignment.ui.main.details.mvp.MovieDetailsPresenter
import com.backbase.assignment.util.onClick
import com.bumptech.glide.Glide
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_movie_details.*
import javax.inject.Inject

const val MOVIE_ID = "MOVIE_ID"

class MovieDetailsFragment : BaseFragment(R.layout.fragment_movie_details),
    MovieDetailsContract.View {

    @Inject
    lateinit var presenter: MovieDetailsPresenter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backButton onClick { (baseActivity as MainActivity).onBackPressed() }

        presenter.getMovieDetails(arguments?.getInt(MOVIE_ID))
    }

    override fun showMovieDetails(details: MovieDetails) {
        title.text = details.title
        releaseDate.text = details.getDisplayReleaseDate()
        overview.text = details.overview
        tagValues.tags = details.genres.map { it.name }

        Glide.with(this).load(details.getPosterLink()).into(detailPoster)
    }

    companion object {
        @JvmStatic
        fun newInstance(movie : Movie) = MovieDetailsFragment().apply {
            arguments = Bundle().apply {
                putInt(MOVIE_ID,movie.id)
            }
        }
    }
}
