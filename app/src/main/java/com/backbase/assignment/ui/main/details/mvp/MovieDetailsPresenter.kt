package com.backbase.assignment.ui.main.details.mvp

import com.backbase.assignment.base.BasePresenter
import com.backbase.assignment.di.api.ApiService
import com.jakewharton.rxrelay2.BehaviorRelay
import com.backbase.assignment.ui.main.details.MovieDetailsFragment
import com.backbase.assignment.util.scheduler.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MovieDetailsPresenter(view: MovieDetailsContract.View,
                            private val apiService: ApiService) :

    BasePresenter<MovieDetailsContract.View>(view), MovieDetailsContract.Presenter {

    override fun getMovieDetails(id: Int?) {
        if (id==null){
            return
        }

        val disposable = apiService.getMovieDetails(id)
            .subscribeBy(
                onSuccess = { view.showMovieDetails(it) },
                onError = { Timber.w(it) }
            )

        addDisposable(disposable)
    }

}
