package com.backbase.assignment.ui.main.details.di


import com.backbase.assignment.ui.main.details.MovieDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MovieDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(MovieDetailsModule::class))
    internal abstract fun provideMovieDetailsFragmentFactory(): MovieDetailsFragment
}
