package com.backbase.assignment.ui.main.details.di

import com.backbase.assignment.di.api.ApiService
import com.backbase.assignment.ui.main.details.MovieDetailsFragment
import com.backbase.assignment.ui.main.details.mvp.MovieDetailsContract
import com.backbase.assignment.ui.main.details.mvp.MovieDetailsPresenter
import dagger.Module
import dagger.Provides

@Module
class MovieDetailsModule {

    @Provides
    internal fun provideView(fragment: MovieDetailsFragment): MovieDetailsContract.View {
        return fragment
    }

    @Provides
    internal fun providePresenter(view: MovieDetailsContract.View, apiService: ApiService):
            MovieDetailsPresenter {

        return MovieDetailsPresenter(view, apiService)
    }
}
