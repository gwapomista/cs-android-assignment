package com.backbase.assignment.ui.main.details.mvp

import com.backbase.assignment.base.BaseView
import com.backbase.assignment.data.api.reponsemodels.MovieDetails

interface MovieDetailsContract {
    interface View : BaseView {
        fun showMovieDetails(details : MovieDetails)
    }

    interface Presenter {
        fun getMovieDetails(id : Int?)
    }
}
