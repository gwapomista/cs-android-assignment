package com.backbase.assignment.ui.main.movie

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.base.BaseActivity
import com.backbase.assignment.data.api.reponsemodels.Movie
import com.backbase.assignment.data.api.reponsemodels.getPosterLink
import com.backbase.assignment.util.create
import com.backbase.assignment.util.inflateChild
import com.backbase.assignment.util.onClick
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_movie_playing.view.*

class PlayingMovieAdapter(private val baseActivity: BaseActivity,
                          private val data : List<Movie>,
                          private val onSelect: (Movie) -> Unit) :
    RecyclerView.Adapter<PlayingMovieAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return parent.create(::ViewHolder, R.layout.adapter_movie_playing)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = data[position]
        holder.itemView.onClick { onSelect(movie) }

        Glide.with(baseActivity).load(movie.getPosterLink()).into(holder.itemView as ImageView)
    }
}