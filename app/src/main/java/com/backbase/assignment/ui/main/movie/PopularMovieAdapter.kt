package com.backbase.assignment.ui.main.movie

import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.base.BaseActivity
import com.backbase.assignment.data.api.reponsemodels.Movie
import com.backbase.assignment.data.api.reponsemodels.getDisplayReleaseDate
import com.backbase.assignment.data.api.reponsemodels.getPosterLink
import com.backbase.assignment.util.create
import com.backbase.assignment.util.onClick
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_movie_popular.view.*


class PopularMovieAdapter(private val baseActivity: BaseActivity,
                          private val data : List<Movie>,
                          private val onSelect: (Movie) -> Unit) :

    RecyclerView.Adapter<PopularMovieAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  parent.create(::ViewHolder, com.backbase.assignment.R.layout.adapter_movie_popular)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = data[position]
        holder.itemView.title.text = movie.title
        holder.itemView.releaseDate.text = movie.getDisplayReleaseDate()
        holder.itemView.ratingView.setRating(movie.vote_average)
        holder.itemView.onClick { onSelect(movie) }

        Glide.with(baseActivity).load(movie.getPosterLink()).into(holder.itemView.poster)


    }
}