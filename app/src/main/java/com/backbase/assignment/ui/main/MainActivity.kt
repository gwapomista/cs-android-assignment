package com.backbase.assignment.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.backbase.assignment.R
import com.backbase.assignment.base.BaseActivity
import com.backbase.assignment.data.api.reponsemodels.Movie
import com.backbase.assignment.ui.main.details.MovieDetailsFragment
import com.backbase.assignment.ui.main.movie.PlayingMovieAdapter
import com.backbase.assignment.ui.main.movie.PopularMovieAdapter
import com.backbase.assignment.ui.main.mvp.MainContract
import com.backbase.assignment.ui.main.mvp.MainPresenter
import com.backbase.assignment.util.activatePagination
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, HasSupportFragmentInjector {

    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }

    @Inject
    lateinit var presenter: MainPresenter

    private lateinit var popularMovieAdapter: PopularMovieAdapter
    private lateinit var playingMovieAdapter: PlayingMovieAdapter
    private lateinit var popularMovies : ArrayList<Movie>
    private lateinit var playingMovies : ArrayList<Movie>

    private var currentPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main)

        initPlayingMovies()
        initPopularMovies()

        presenter.getCachedMovies()
        presenter.fetchPlayingMovies()
        presenter.fetchPopularMovies(currentPage)
    }

    override fun displayCachedPlayingMovies(movies: List<Movie>) {
        playingMovies.addAll(movies)
        playingMovieAdapter.notifyDataSetChanged()
    }

    override fun displayCachedPopularMovies(movies: List<Movie>) {
        playingMovies.addAll(movies)
        playingMovieAdapter.notifyDataSetChanged()
    }

    override fun showPlayingMovies(movies: List<Movie>) {
        playingMovies.clear()
        playingMovies.addAll(movies)
        playingMovieAdapter.notifyDataSetChanged()
    }

    override fun showPopularMovies(movies: List<Movie>) {
        popularMovies.clear()
        popularMovies.addAll(movies)
        popularMovieAdapter.notifyDataSetChanged()

        scrollView.activatePagination{ presenter.fetchPopularMovies(currentPage) }

        currentPage = 1
        incrementPage()
    }

    override fun addPopularMovies(movies: List<Movie>) {
        popularMovies.addAll(movies)
        popularMovieAdapter.notifyDataSetChanged()

        incrementPage()
    }

    private fun initPlayingMovies(){
        playingMovies = ArrayList()
        playingMovieAdapter = PlayingMovieAdapter(this,playingMovies){
            showMovieDetail(it)
        }

        rvPlayingMovies.adapter = playingMovieAdapter
        rvPlayingMovies.layoutManager = LinearLayoutManager(
            this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun initPopularMovies(){
        popularMovies = ArrayList()
        popularMovieAdapter = PopularMovieAdapter(this,popularMovies){
            showMovieDetail(it)
        }

        rvPopularMovies.adapter = popularMovieAdapter
        rvPopularMovies.layoutManager = LinearLayoutManager(
            this, LinearLayoutManager.VERTICAL, false)
    }

    private fun incrementPage(){
        currentPage += 1
    }

    private fun showMovieDetail(movie: Movie){
        detailContainer.visibility = View.VISIBLE
        replaceFragment(MovieDetailsFragment.newInstance(movie),R.id.detailContainer)
    }

    override fun onBackPressed() {
        detailContainer.visibility = View.GONE
    }
}
