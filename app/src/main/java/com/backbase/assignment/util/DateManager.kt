package com.backbase.assignment.util

import java.text.SimpleDateFormat


const val JSON_DATE_FORMAT = "yyyy-MM-dd"
const val PATTERN_DATE_DISPLAY = "MMMM dd, yyyy"

class DateManager

fun String.changePattern(from: String,to: String): String {
    if (this.isNullOrEmpty()){
        return ""
    }

    val formatter = SimpleDateFormat(from)
    return SimpleDateFormat(to).format(formatter.parse(this))
}