package com.backbase.assignment.util

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.annotation.LayoutRes
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.view.RxView
import java.util.concurrent.TimeUnit

class UIManager

@SuppressLint("CheckResult")
internal infix fun View.onClick(function: () -> Unit) {
    RxView.clicks(this).throttleFirst(100, TimeUnit.MILLISECONDS)
        .subscribe { function() }
}

inline fun <reified T : View> ViewGroup.inflateChild(resourceId: Int): T {
    val inflater = LayoutInflater.from(context)
    val view = inflater.inflate(resourceId, this, false)
    addView(view)
    return view as T
}

inline fun <reified T: RecyclerView.ViewHolder> ViewGroup.create(createHolder: (View) -> T, @LayoutRes res: Int): T {
    val inflater = LayoutInflater.from(context)
    val itemView = inflater.inflate(res, this, false)
    return createHolder(itemView)
}

fun NestedScrollView.activatePagination(onPaginate: () -> Unit){
    val listener = ViewTreeObserver.OnScrollChangedListener {
        if (this.getChildAt(0).bottom <= (this.height + this.scrollY)){
            onPaginate()
        }
    }

    this.viewTreeObserver.addOnScrollChangedListener(listener)
}
