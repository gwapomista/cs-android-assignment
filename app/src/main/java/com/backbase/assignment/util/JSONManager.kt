package com.backbase.assignment.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken

object JSONManager {
    private val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"

    fun <T> fromJson(json: String, classOfT: Class<T>): T? {
        val gson = GsonBuilder().setDateFormat(dateFormat)
            .create()

        try {
            return gson.fromJson(json, classOfT)
        } catch (e: Exception) {
            return null
        }

    }

    fun toJson(`object`: Any): String {
        val gson = GsonBuilder().setDateFormat(dateFormat)
            .registerTypeAdapterFactory(object : TypeAdapterFactory {
                override fun <T> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T>? {
                    return null
                }
            })
            .create()
        return gson.toJson(`object`)
    }

    fun toJson(values: List<String>): String {
        val gson = GsonBuilder().setDateFormat(dateFormat).create()
        return gson.toJson(values)
    }

    fun fromJson(value: String): List<String>? {
        val gson = GsonBuilder().setDateFormat(dateFormat).create()
        return gson.fromJson<List<String>>(value, object : TypeToken<List<String>>() {

        }.type)
    }

}
