package com.backbase.assignment.util

import java.util.regex.Pattern

class ValidationManager

fun String.isNumeric() : Boolean{
    return matches("-?\\d+(\\.\\d+)?".toRegex())
}

fun String.isEmail() : Boolean{
    return Pattern.compile(
        "^[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\$"
    ).matcher(this.toLowerCase()).matches()
}

fun String.isPhilippineMobileNo() : Boolean{
    if (this.isEmpty() || this == "+63"){
        return false
    }

    return this.length == 11 || (this.length == 10 && this.startsWith("9"))
}