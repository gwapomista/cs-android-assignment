package com.backbase.assignment.data.api.reponsemodels

import com.backbase.assignment.BuildConfig
import com.backbase.assignment.util.JSONManager
import com.backbase.assignment.util.JSON_DATE_FORMAT
import com.backbase.assignment.util.PATTERN_DATE_DISPLAY
import com.backbase.assignment.util.changePattern

class MovieResponse (
    val dates : Dates,
    val results : List<Movie>,
    val page : Int,
    val total_pages : Int,
    val total_results : Int
)

class Dates (
    val maximum: String,
    val minimum: String
)

class Movie(
    val id : Int,
    val popularity : Double,
    val overview: String,
    val title : String,
    val genre_ids : List<Int>,
    val backdrop_path : String,
    val original_language : String,
    val original_title : String,
    val poster_path : String,
    val release_date : String?,
    val video : Boolean,
    val vote_average : Double,
    val vote_count : Int
)

class MovieDetails (
    val id : Int,
    val popularity : Double,
    val overview: String,
    val title : String,
    val genre_ids : List<Int>,
    val backdrop_path : String,
    val original_language : String,
    val original_title : String,
    val poster_path : String,
    val release_date : String?,
    val video : Boolean,
    val vote_average : Float,
    val vote_count : Int,
    val genres : List<MovieGenre>
)

class MovieGenre (
    val id : Int,
    val name : String
)

fun Movie.getPosterLink(): String{
    return BuildConfig.IMAGE_BUCKET + poster_path
}

fun MovieDetails.getPosterLink(): String{
    return BuildConfig.IMAGE_BUCKET + poster_path
}

fun Movie.getDisplayReleaseDate() : String{
    return release_date?.changePattern(JSON_DATE_FORMAT, PATTERN_DATE_DISPLAY)?: ""
}

fun MovieDetails.getDisplayReleaseDate() : String{
    return release_date?.changePattern(JSON_DATE_FORMAT, PATTERN_DATE_DISPLAY)?: ""
}


//Cache Objects
class MoviesCache(val movies : List<Movie>)

fun MoviesCache.toRaw() : String{
    return JSONManager.toJson(this)
}

fun String.toMovieCache() : MoviesCache{
    return JSONManager.fromJson(this,MoviesCache::class.java)?: MoviesCache(ArrayList())
}
