package com.backbase.assignment.data.api.reponsemodels

class ContentWrapper<T>(
    val content: T
)