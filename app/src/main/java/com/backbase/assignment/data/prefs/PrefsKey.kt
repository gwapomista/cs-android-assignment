package com.backbase.assignment.data.prefs

enum class PrefsKey {
    IS_ALREADY_LAUNCHED,
    POPULAR_MOVIE_CACHE,
    PLAYING_MOVIE_CACHE
}
