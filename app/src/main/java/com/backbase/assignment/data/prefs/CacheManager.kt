package com.backbase.assignment.data.prefs

import android.content.Context
import android.content.SharedPreferences
import com.backbase.assignment.data.api.reponsemodels.Movie
import com.backbase.assignment.data.api.reponsemodels.MoviesCache
import com.backbase.assignment.data.api.reponsemodels.toMovieCache
import com.backbase.assignment.data.api.reponsemodels.toRaw
import javax.inject.Inject

class CacheManager @Inject
constructor(context: Context) {

    private val SHARED_PREF_NAME = "appp"
    private val mPref: SharedPreferences?
    private var mEditor: SharedPreferences.Editor? = null

    init {
        mPref = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    fun clear() {
        mPref!!.edit().clear().apply()
    }

    fun save(key: String, value: String): Boolean {
        if (mPref != null) {
            mEditor = mPref.edit()
            mEditor!!.putString(key, value)
            return mEditor!!.commit()
        } else {
            return false
        }
    }

    fun save(key: String, value: Boolean): Boolean {
        if (mPref != null) {
            mEditor = mPref.edit()
            mEditor!!.putString(key, value.toString())
            return mEditor!!.commit()
        } else {
            return false
        }
    }

    operator fun get(key: String): String {
        if (mPref != null) {
            val value = mPref.getString(key, "")?: ""
            return if (value == "") "" else value
        } else {
            return ""
        }
    }

    fun getBoolean(key: String): Boolean {
        if (mPref != null) {
            val value = mPref.getString(key, "")?: ""
            return if (value == "") false else value.toBoolean()
        } else {
            return false
        }
    }

    fun savePopularMovies(movies : List<Movie>){
        save(PrefsKey.POPULAR_MOVIE_CACHE.name,MoviesCache(movies).toRaw())
    }

    fun savePlayingMovies(movies: List<Movie>){
        save(PrefsKey.PLAYING_MOVIE_CACHE.name,MoviesCache(movies).toRaw())
    }

    fun getPlayingMoviesCache() : List<Movie>{
        return get(PrefsKey.PLAYING_MOVIE_CACHE.name).toMovieCache().movies
    }

    fun getPopularMoviesCache() : List<Movie>{
        return get(PrefsKey.POPULAR_MOVIE_CACHE.name).toMovieCache().movies
    }
}
