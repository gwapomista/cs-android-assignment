package com.backbase.assignment.di.api

import com.backbase.assignment.data.api.reponsemodels.MovieDetails
import com.backbase.assignment.data.api.reponsemodels.MovieResponse
import io.reactivex.Single
import retrofit2.http.*

interface ApiInterface {

    @GET("movie/now_playing")
    fun getCurrentlyPlayingMovies(
        @Query("language") language: String,
        @Query("page") page: String,
        @Query("api_key") apiKey: String
    ): Single<MovieResponse>

    @GET("movie/popular")
    fun getPopularMovies(
        @Query("page") page: Int,
        @Query("api_key") apiKey: String
    ): Single<MovieResponse>

    @GET("movie/{id}")
    fun getMovieDetails(
        @Path("id") id: Int,
        @Query("api_key") apiKey: String
    ): Single<MovieDetails>

}
