package com.backbase.assignment.di.api

import com.backbase.assignment.BuildConfig
import com.backbase.assignment.data.api.reponsemodels.MovieDetails
import com.backbase.assignment.data.api.reponsemodels.MovieResponse
import com.backbase.assignment.util.scheduler.AppSchedulerProvider
import io.reactivex.Single

class ApiService(private val apiInterface: ApiInterface,
                 private val schedulerProvider: AppSchedulerProvider) {

    fun getPlayingMovies(): Single<MovieResponse> {
        return apiInterface.getCurrentlyPlayingMovies(
            "en-us","undefined",BuildConfig.API_KEY)
            .observeOn(schedulerProvider.ui())
            .subscribeOn(schedulerProvider.io())
    }

    fun getPopularMovies(page : Int): Single<MovieResponse> {
        return apiInterface.getPopularMovies(page,BuildConfig.API_KEY)
            .observeOn(schedulerProvider.ui())
            .subscribeOn(schedulerProvider.io())
    }

    fun getMovieDetails(id : Int): Single<MovieDetails> {
        return apiInterface.getMovieDetails(id,BuildConfig.API_KEY)
            .observeOn(schedulerProvider.ui())
            .subscribeOn(schedulerProvider.io())
    }
}