package com.backbase.assignment.di

import com.backbase.assignment.ui.main.MainActivity
import com.backbase.assignment.ui.main.details.di.MovieDetailsFragmentProvider
import com.backbase.assignment.ui.main.di.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = arrayOf(MainModule::class,MovieDetailsFragmentProvider::class))
    internal abstract fun bindMainActivity(): MainActivity

}
