package com.backbase.assignment.di

import android.content.Context
import com.backbase.assignment.BuildConfig
import com.backbase.assignment.base.BaseApp
import com.backbase.assignment.data.prefs.CacheManager
import com.backbase.assignment.di.api.ApiInterface
import com.backbase.assignment.di.api.ApiService
import com.backbase.assignment.util.scheduler.AppSchedulerProvider
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: BaseApp): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(application: BaseApp): OkHttpClient {
        /** initialize ok http client  */
        var cacheDir = application.externalCacheDir
        if (cacheDir == null) {
            cacheDir = application.cacheDir
        }
        val cache = Cache(cacheDir!!, (10 * 1024 * 1024).toLong())

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .cache(cache)
            .connectTimeout(CONNECTION_TIME_OUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(READ_TIME_OUT.toLong(), TimeUnit.SECONDS)
            .build()
    }


    @Provides
    @Singleton
    fun provideRetrofit(application: BaseApp): Retrofit {
        val gson = GsonBuilder().setLenient().create()

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.HOST_NAME)
                .client(provideOkHttpClient(application))
                .build()
    }

    @Provides
    @Singleton
    fun providePrefs(application: BaseApp): CacheManager {
        return CacheManager(application)
    }

    @Provides
    @Singleton
    fun provideAppSchedulerProvider(): AppSchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideApiInterface(application: BaseApp): ApiInterface {
        return provideRetrofit(application).create<ApiInterface>(
            ApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideUsersService(apiInterface: ApiInterface, appSchedulerProvider: AppSchedulerProvider): ApiService {
        return ApiService(apiInterface, appSchedulerProvider)
    }

    companion object {
        private const val CONNECTION_TIME_OUT = 60
        private const val READ_TIME_OUT = 60
    }
}
